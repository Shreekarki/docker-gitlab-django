FROM python:3.8.2-slim-buster

# set work directory
WORKDIR /dev_portfolio

# set enviroment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
COPY requirements.txt /dev_portfolio/
RUN pip install -r requirements.txt

#copy project
COPY . /dev_portfolio/
